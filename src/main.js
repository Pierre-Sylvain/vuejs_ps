// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

//tuto
//https://bootstrap-vue.js.org/docs/
//https://fr.vuejs.org/v2/guide/index.html
//https://xaksis.github.io/vue-good-table/guide/#basic-example
import Vue from 'vue'
import App from './App'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { 
    App
   },
  template: '<App/>'
})
